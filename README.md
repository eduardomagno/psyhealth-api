# Psy Health json server API.

Essa Api fake foi feita com json server para o MVP do Psy Health.

## Base URL

```
	https://psy-health-api.herokuapp.com/
```

## Base Header

```
	Content-Type: application/json
```

## Endpoints

### GET /users

> retorna todos os usuários cadastrados

response:

```json
{
  "users": [
    {
      "email": "joanacamargo@gmail.com",
      "password": "$2a$10$c/7bh67upWNx4x3zevtkg.kbgzcLrZkHaab065brPAKmePmBrLhY6",
      "name": "Joana Camargo",
      "is_psic": true,
      "phone": "(41)92979-8798",
      "cpf_cnpj": "016.5s8.123.18",
      "crp": "02/23231",
      "id": 1
    },
    {
      "email": "joaodasilva@gmail.com",
      "password": "$2a$10$NinIKsv9cluF8vNFgXU0ountbuIvFAzVMatlc3H3AO3MHgMsizrsG",
      "name": "João da Silva",
      "is_psic": false,
      "phone": "(41)98979-8798",
      "cpf_cnpj": "016.538.123.18",
      "id": 2
    }
  ]
}
```

### GET /users/:id

> retorna o usuário específico

response:

```json
{
  "email": "joaodasilva@gmail.com",
  "password": "$2a$10$NinIKsv9cluF8vNFgXU0ountbuIvFAzVMatlc3H3AO3MHgMsizrsG",
  "name": "João da Silva",
  "is_psic": false,
  "phone": "(41)98979-8798",
  "cpf_cnpj": "016.538.123.18",
  "id": 2
}
```

### POST /login

> login do paciente e psicólogo

body:

```json
{
  "email": "email@email.com",
  "password": "123456"
}
```

### POST /register

> registro do paciente

body:

```json
{
  "name": "João da Silva",
  "is_psic": false,
  "email": "joaodasilva@gmail.com",
  "phone": "(41)98979-8798",
  "cpf_cnpj": "016.538.123.18",
  "password": "123456"
}
```

> registro do psicólogo

body:

```json
{
  "name": "Joana Camargo",
  "is_psic": true,
  "email": "joanacamargo@gmail.com",
  "phone": "(41)92979-8798",
  "cpf_cnpj": "016.5s8.123.18",
  "crp": "02/23231",
  "password": "123456"
}
```

> quando o registro é bem sucedido você recebe um token

response:

```json
{
  "auth_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IlJhZmFlbEdNMkBnbWFpbC5jb20iLCJpYXQiOjE2MDE0NzM5MTMsImV4cCI6MTYwMTQ3NzUxMywic3ViIjoiMyJ9.K3rfDWw5JuKyFmsPO5h1VJ6W8NM-Xz6am8XZzvAS5JQ"
}
```

## PATCH /users/:id

#### Importante

**para a requisição dê certo, é necessário passar o id do usuário que está sendo modificado, utilizando a prop `userId`**

**No header quando for utilizar o token, é necessário adicionar a palavra `Bearer` antes para que funcione corretamente**

> patchs possiveis para pacientes

header:

```
	Content-Type: application/json
	Authorization: Bearer <TOKEN>
```

body:

```json
{
  "userId": "1",
  "name": "Rodisvaldo Pereira",
  "email": "rodisvaldo.psicologo@gmail.com",
  "phone": "(41)98179-0790",
  "cpf_cnpj": "016.038.103.11",
  "password": "123abc",
  "appointments": [
    {
      "id": 0,
      "date": {
        "start": "2020-09-03 10:00:00",
        "end": "2020-09-03 11:00:00"
      },
      "psic": {
        "name": "Rodisval Pereira",
        "id": 0
      }
    },
    {
      "id": 1,
      "date": {
        "start": "2020-10-02 15:00:00",
        "end": "2020-10-02 16:00:00"
      },
      "psic": {
        "name": "Rodisval Pereira",
        "id": 0
      }
    }
  ]
}
```

> patchs disponiveis para psicólogo

_além de ser possivel alterar as propriedades que um usuário comum tem, é possivel alterar/adicionar as seguintes propriedades:_

```json
	"crp": "01/901292",
 	"specializations": [
      "psicologia infantil",
      "psicologia de casais"
		],
	"workDays": {
    "1": [7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19],
    "2": [8, 9, 10],
    "5": [13, 14, 15, 16, 17, 18, 19]
  },

```

header:

```
	Content-Type: application/json
	Authorization: Bearer <TOKEN>
```

body:

```json
{
  "userId": 2,
  "name": "Rodisval Pereira",
  "email": "rodisval.psicologo@gmail.com",
  "phone": "(41)98745-2365",
  "cpf-cnpj": "112.521.322.15",
  "crp": "01/901292",
  "password": "123abc",
  "image": "https://media.vittude.com/media/profile_photos/psicologo-jose-alan-martins-de-freitas_5MAeAhE.jpg",
  "specializations": ["psicologia infantil", "psicologia de casais"],
  "workDays": {
    "1": [7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19],
    "2": [8, 9, 10],
    "5": [13, 14, 15, 16, 17, 18, 19]
  }
}
```

### GET /appointments

> retorna todos os appointments

response:

```json
[
  {
    "userId": 1,
    "teste": "test",
    "id": 1,
    "addnewprops": "addnewpropssd",
    "date": {
      "start": "2020-10-02 14:00:00",
      "end": "2020-10-02 16:00:00"
    },
    "patient": {
      "name": "João da Silva",
      "id": 1
    },
    "psic": {
      "name": "Priscila Mota",
      "id": 1
    }
  },
  {
    "userId": 1,
    "id": 2,
    "date": {
      "start": "2020-10-02 15:00:00",
      "end": "2020-10-02 16:00:00"
    },
    "patient": {
      "name": "João da Silva",
      "id": 1
    },
    "psic": {
      "name": "Priscila Mota",
      "id": 1
    }
  }
]
```

### GET /appointments/:id

> retorna um appointment especifico

response:

```json
{
  "userId": 1,
  "id": 2,
  "date": {
    "start": "2020-10-02 15:00:00",
    "end": "2020-10-02 16:00:00"
  },
  "patient": {
    "name": "João da Silva",
    "id": 1
  },
  "psic": {
    "name": "Priscila Mota",
    "id": 1
  }
}
```

### POST /appointments

> adicionando um novo appoitment

#### Importante

**\*É necessário adicionar o `id` do appoint manualmente, além de adicionar um `userId` para que o appointment perntença a algum usuário**

header:

```
	Content-Type: application/json
	Authorization: Bearer <TOKEN>
```

body:

```json
{
  "userId": 1,
  "id": 2,
  "date": {
    "start": "2020-10-02 15:00:00",
    "end": "2020-10-02 16:00:00"
  },
  "patient": {
    "name": "João da Silva",
    "id": 1
  },
  "psic": {
    "name": "Priscila Mota",
    "id": 1
  }
}
```

### PATCH /appointments/:id

> alterando alguma propriedade do appointment

header:

```
	Content-Type: application/json
	Authorization: Bearer <TOKEN>
```

body:

```json
{
  "userId": 1,
  "date": {
    "start": "2020-10-02 14:00:00",
    "end": "2020-10-02 15:00:00"
  }
}
```


### DELETE /appointments/:id

> excluindo um appointment

header:

```
	Content-Type: application/json
	Authorization: Bearer <TOKEN>
```


### GET /depoiments

response:

```json
[
  {
    "userId": 12,
    "coment": "Muito feliz com o resultado das sessões. Tinha um pé atrás com o atendimento online e consegui ficar muito à vontade com o especialista.",
    "grading": 5,
    "psiId": 3,
    "appointmentId": 7,
    "id": 1
  },
  {
    "userId": 12,
    "coment": "Agradeço à Drª que conseguiu identificar o principal motivo do meu caso e, com a sua experiência, me fez pensar racionalmente. Juntos, buscamos soluções para que consiga resolvê-lo da melhor forma.",
    "grading": 4.5,
    "psiId": 1,
    "appointmentId": 8,
    "id": 2
  },
]

```


### POST /depoiments

header:

```
	Content-Type: application/json
	Authorization: Bearer <TOKEN>
```
body:

```json
{
    "userId": 12,
    "coment": "Muito feliz com o resultado das sessões. Tinha um pé atrás com o atendimento online e consegui ficar muito à vontade com o especialista.",
    "grading": 5,
    "psiId": 3,
    "appointmentId": 7,
    "id": 1
  }
```